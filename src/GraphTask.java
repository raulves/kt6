import java.io.BufferedReader;
import java.io.IOException;
import java.util.*;
import java.io.*;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   LinkedHashMap<Vertex, Map<Vertex, Arc>> distanceMatrix;

   /** Main method. */
   public static void main (String[] args) throws IOException {
      GraphTask a = new GraphTask();
      a.run();
   }

   /** Actual main method to run examples and everything. */
   public void run() throws IOException {


      Graph g = new Graph ("SimpleGraph");

      Vertex a = new Vertex("A");
      Vertex b = new Vertex("B");
      Vertex c = new Vertex("C");
      Vertex d = new Vertex("D");
      Vertex e = new Vertex("E");

      g.first = a;
      a.next = b;
      b.next = c;
      c.next = e;
      e.next = d;

      Arc ab = new Arc("AB", 6);
      Arc ba = new Arc("BA", 6);

      Arc ad = new Arc("AD", 1);
      Arc da = new Arc("DA", 1);

      Arc de = new Arc("DE", 1);
      Arc ed = new Arc("ED", 1);

      Arc be = new Arc("BE", 2);
      Arc eb = new Arc("EB", 2);

      Arc db = new Arc("DB", 2);
      Arc bd = new Arc("BD", 2);

      Arc bc = new Arc("BC", 5);
      Arc cb = new Arc("CB", 5);

      Arc ec = new Arc("EC", 5);
      Arc ce = new Arc("CE", 5);

      a.first = ad;
      ad.target = d;
      ad.next = ab;
      ab.target = b;

      b.first = ba;
      ba.target = a;
      ba.next = bd;
      bd.target = d;
      bd.next = be;
      be.target = e;
      be.next = bc;
      bc.target = c;

      c.first = cb;
      cb.target = b;
      cb.next = ce;
      ce.target = e;

      e.first = ec;
      ec.target = c;
      ec.next = eb;
      eb.target = b;
      eb.next = ed;
      ed.target = d;

      d.first = de;
      de.target = e;
      de.next = db;
      db.target = b;
      db.next = da;
      da.target = a;

      System.out.println (g);

      Dijkstra dijkstra = new Dijkstra();
      dijkstra.outputShortestPathTo(g, a, c);


      List<Arc> bestRoute = getBestRoute(g, a);

      System.out.println("Best route is: " + bestRoute);

      int routeLength = bestRoute.stream().mapToInt(number -> number.weight).sum();
      System.out.println("Route length: " + routeLength + " units");



      Graph pageGraph = createPageGraph("testData/SvenAntonGraph");
      Vertex start = pageGraph.getVertexById("moodle.ee");
      Vertex end = pageGraph.getVertexById("epood.ee");

      System.out.println (pageGraph);

      Dijkstra dijkstraFile = new Dijkstra();
      dijkstraFile.outputShortestPathTo(pageGraph, start, end);


      List<Arc> bestRoute2 = getBestRoute(pageGraph, start);

      System.out.println("Best route is: " + bestRoute2);

      int routeLength2 = bestRoute.stream().mapToInt(number -> number.weight).sum();
      System.out.println("Route length: " + routeLength2 + " units");


   }

   // Sven Anton-i kood
   private static BufferedReader bufferCSV(String filePath) throws FileNotFoundException {
      File file = new File(filePath);
      return new BufferedReader(new FileReader(file));
   }


   private static void checkCSV(String row, String filePath, int counter) {
      String[] commaSplit = row.split(",");
      if (commaSplit.length < 2 || (!commaSplit[1].startsWith("\"") && commaSplit.length > 2)) {
         throw new IllegalArgumentException(String.format("" +
                         "\n\nThe row '%s' in file '%s'(row nr %s) is invalid. \nProbably there is a missing" +
                         "comma or missing links list. \nPlease note that" +
                         "even empty list has to be present as an argument.\n",
                 row, filePath, counter));
      }
   }

   private static LinkedList<String> createStack(String row) {
      return new LinkedList<> (Arrays.asList(
              row.replaceAll("\"", "").split(",")));
   }

   public Graph createPageGraph(String filePath) throws IOException {
      Graph pageLink = new Graph("PageLink");
      BufferedReader br = bufferCSV(filePath);
      String row;
      int counter = 0;

      while ((row = br.readLine()) != null) {
         if (++counter == 1) { continue;}
         checkCSV(row, filePath, counter);

         LinkedList<String> rowStack = createStack(row);
         Vertex origin = pageLink.setVertex(rowStack.pop());
         Vertex target = pageLink.setVertex(rowStack.pop());
         Arc link = pageLink.initializeLink(origin, target);
         origin.first = link;

         if (rowStack.size() > 0) pageLink.setArcs(rowStack, origin, link);
      }
      return pageLink;
   }

   // Sven Anton-i koodi lõpp



   /**
    * Returns an adjacency matrix from the graph
    *
    * @param  g any Graph object
    * @return      adjacency matrix
    */
   private LinkedHashMap<Vertex, Map<Vertex, Arc>> distanceMatrix( Graph g ){
      LinkedHashMap<Vertex, Map<Vertex, Arc>> distanceMatrix = new LinkedHashMap<>();
      Vertex currentPoint = g.first;
      while( currentPoint != null ){
         HashMap<Vertex, Arc> distances = new HashMap<>();
         Arc currentPath = currentPoint.first;
         while( currentPath != null ){
            distances.put( currentPath.target, currentPath );
            currentPath = currentPath.next;
         }
         distanceMatrix.put( currentPoint, distances );
         currentPoint = currentPoint.next;
      }
      return distanceMatrix;
   }


   /**
    * Returns a list of vertex lists which contain all permutations of all possible paths from original list of vertexes
    *
    * @param  original  List of vertexes
    * @return      List of vertex lists(all possible paths)
    */
   private <Vertex> List<List<Vertex>> generatePermutations( List<Vertex> original ){
      if( original.isEmpty() ){
         List<List<Vertex>> result = new ArrayList<>();
         result.add( new ArrayList<>() );
         return result;
      }
      Vertex firstElement = original.remove( 0 );
      List<List<Vertex>> returnValue = new ArrayList<>();
      List<List<Vertex>> permutations = generatePermutations( original );
      for( List<Vertex> smallerPermutated : permutations ){
         for( int index = 0; index <= smallerPermutated.size(); index++ ){
            List<Vertex> temp = new ArrayList<>( smallerPermutated );
            temp.add( index, firstElement );
            returnValue.add( temp );
         }
      }
      return returnValue;
   }


   /**
    * Returns distance from starting vertex going through all vertexes in the route.
    *
    * @param  startingPoint  Starting Vertex
    * @param  route  List of vertexes - path of vertexes to go through
    * @return      long - distance from starting point
    */
   long getDistance( Vertex startingPoint, List<Vertex> route ){
      if(route.size() == 0) return 0;

      long sum = 0;
      Vertex source = startingPoint;
      for( Vertex target : route ){
         sum += distanceMatrix.get( source ).get( target ).weight;
         source = target;
      }
      return sum + distanceMatrix.get( source ).get( startingPoint ).weight;
   }


   /**
    * Returns list of arcs , arcs that are used while moving from starting vertex going through all vertexes in the route.
    *
    * @param  startingPoint  Starting Vertex
    * @param  route  List of vertexes - path of vertexes to go through
    * @return      List of arcs that are in the path
    */
   List<Arc> toArcs( Vertex startingPoint, List<Vertex> route ){
      if(route.size() == 0) return Collections.emptyList();

      ArrayList<Arc> arcs = new ArrayList<>();
      Vertex previous = startingPoint;
      for( Vertex target : route ){
         Arc arc = distanceMatrix.get( previous ).get( target );
         arcs.add( arc );
         previous = target;
      }

      arcs.add( distanceMatrix.get( previous ).get( startingPoint ) );
      return arcs;
   }

   /**
    * Returns list of arcs that are in the fastest route.
    *
    * @param  g  Graph object
    * @return      List of arcs that are in the best route.
    */
   public List<Arc> getBestRoute( Graph g, Vertex startingPoint ){
      distanceMatrix = distanceMatrix( g );
      ArrayList<Vertex> cities = new ArrayList<>( distanceMatrix.keySet() );

      List<List<Vertex>> permutations = generatePermutations( cities.subList( 1, cities.size() ) );
      List<Vertex> vertexRoute = bestRoute( startingPoint, permutations );
      return toArcs( startingPoint, vertexRoute );
   }


   /**
    * Returns list of vertexes that are in the shortest route from list of all possibles routes from that vertex.
    *
    * @param  startingPoint  Starting Vertex
    * @param  permutations  List of all possible permutations of path
    * @return      List of vertexes that are in the best route.
    */
   private List<Vertex> bestRoute( Vertex startingPoint, List<List<Vertex>> permutations ){
      long minimumDistance = Long.MAX_VALUE;
      List<Vertex> bestRoute = new ArrayList<>();

      for( List<Vertex> permutation : permutations ){
         // Checking is route possible in the graph
         if (!isRoutePossible(startingPoint, permutation)) continue;
         long summaryDistance = getDistance( startingPoint, permutation );
         if( summaryDistance < minimumDistance ){
            minimumDistance = summaryDistance;
            bestRoute = permutation;
         }

      }
      return bestRoute;
   }

   private boolean isRoutePossible(Vertex startingPoint, List<Vertex> route)  {
      if(route.size() == 0) return false;
      Vertex source = startingPoint;
      for(Vertex target : route){
         if (distanceMatrix.get(source).get(target) == null) return false;
         source = target;
      }
      if (distanceMatrix.get(source).get(startingPoint) == null) return false;
      return true;
   }


   /**
    * Vertex represents a node/point in the graph. Which is connected by edges.
    */
   class Vertex implements Comparable {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;
      // You can add more fields, if needed
      int minDistance;
      Vertex previous;

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
         minDistance = Graph.INFINITY;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      @Override
      public int compareTo(Object o) {
         Vertex other = (Vertex) o;
         return Integer.compare(this.minDistance, other.minDistance);
      }
   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      // You can add more fields, if needed
      int weight;

      Arc (String s, Vertex v, Arc a, int length) {
         id = s;
         target = v;
         next = a;
         weight = length;
      }

      Arc (String s, int length) {
         this (s, null, null, length);
      }

      Arc(Vertex v, int weight) {
         this(null, v, null, weight);
      }

      Arc(String s) {
         this (s, null, null, 2);
      }

      @Override
      public String toString() {
         return id;
      }

   }

   /**
    * Graph
    */
   class Graph {

      static  final int INFINITY = Integer.MAX_VALUE / 4;

      private String id;
      private Vertex first;
      private int info = 0;
      // You can add more fields, if needed
      // Sven Anton-i kood
      LinkedList<String> pageCache = new LinkedList<>();

      public void addToCache(String vertexName) {
         this.pageCache.add(vertexName);
      }

      public boolean pageIsCached(String vertexName) {
         return this.pageCache.contains(vertexName);
      }

      public Vertex getVertexById(String id) {
         Vertex graphVertex = this.first;
         while (graphVertex != null) {
            if (graphVertex.id.equals(id)) { return graphVertex; }
            graphVertex = graphVertex.next;
         }
         return null;
      }

      public Vertex setVertex(String vertexName) {
         if (!this.pageIsCached(vertexName))
         {
            this.addToCache(vertexName);
            return this.createVertex(vertexName);
         }
         else { return this.getVertexById(vertexName); }
      }

      public Arc initializeLink(Vertex origin, Vertex target) {
         Arc link = new Arc("{"+origin + "->" + target+"}");
         link.target = target;
         return link;
      }

      public void setArcs(LinkedList<String> rowStack, Vertex origin, Arc link) {
         while (rowStack.size() != 0)
         {
            Vertex nextTarget = this.setVertex(rowStack.pop());
            Arc nextLink = this.initializeLink(origin, nextTarget);
            link.next = nextLink;
            link = nextLink;
         }
      }

      // Sven Anton-i koodi lõpp

      Graph (String s, Vertex v) {
         id = s;
         first = v;
         LinkedList<String> pageCache = new LinkedList<>();
      }

      Graph (String s) {
         this (s, null);
      }

      /**
       * Selle meetodi implementeerimiseks olen kasutanud Andres Käver-i tööd, "Individuaaltöö aines Argoritmid ja andmestruktuurid"
       */
      public void resetPathInfo() {
         Vertex v = first;
         while (v != null) {
            v.minDistance = INFINITY;
            v.previous = null;
            v = v.next;
         }
      }

      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

      public ArrayList<Vertex> getVertexList() {
         ArrayList<Vertex> res = new ArrayList<>();
         Vertex v = first;
         while (v != null) {
            res.add(v);
            v = v.next;
         }
         return res;
      }

      /**
       * Selle meetodi implementeerimiseks olen kasutanud Andres Käver-i tööd, "Individuaaltöö aines Argoritmid ja andmestruktuurid"
       */
      public PriorityQueue<Vertex> getVertexPriorityQueue() {
         PriorityQueue<Vertex> res = new PriorityQueue<>(1, shortestDistanceComparator);
         Vertex v = first;
         while (v != null) {
            res.add(v);
            v = v.next;
         }
         return res;
      }

      /**
       * comparator for priority queue sorting
       * Selle meetodi implementeerimiseks olen kasutanud Andres Käver-i tööd, "Individuaaltöö aines Argoritmid ja andmestruktuurid"
       */
      private final Comparator<Vertex> shortestDistanceComparator = (v1, v2) -> {
         if (v1.minDistance > v2.minDistance) {
            return 1;
         }
         if (v1.minDistance < v2.minDistance) {
            return  -1;
         }
         return 0;
      };


      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                       + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                       + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException
                    ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j)
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0)
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }
   }


   /**
    * Dijkstra algorithm
    */
   class Dijkstra
   {
      /**
       * Selle meetodi implementeerimiseks olen kasutanud Andres Käver-i tööd, "Individuaaltöö aines Argoritmid ja andmestruktuurid"
       * Generates shortest paths according to the starting vertex.
       * @param graph Given graph
       * @param startingVertex Starting vertex, vertex that is the starting point of calculating shortest paths from.
       */
      public void generateShortestPaths(Graph graph, Vertex startingVertex) {
         if (graph == null) {
            throw new RuntimeException("Dijkstra: graph is null!");
         }
         if (graph.first == null) {
            throw new RuntimeException("Dijkstra: graph is empty.");
         }
         if (!graph.getVertexList().contains(startingVertex)) {
            throw new RuntimeException("Dijkstra: starting vertex not found in given graph!");
         }
         graph.resetPathInfo();
         startingVertex.minDistance = 0;
         startingVertex.previous = startingVertex;

         PriorityQueue<Vertex> verticesQ = graph.getVertexPriorityQueue();

         while (!verticesQ.isEmpty()) {
            Vertex shortestDistanceVertex = verticesQ.poll();

            if(shortestDistanceVertex.minDistance == Graph.INFINITY) {
               throw new RuntimeException("Dijkstra: min vertex(" + shortestDistanceVertex + ") length can not be infinity.");
            }
            if (shortestDistanceVertex.minDistance < 0) {
               throw new RuntimeException("Dijkstra: min vertex(" + shortestDistanceVertex + ") length can not be negative.");
            }

            Arc arc = shortestDistanceVertex.first;
            while (arc != null) {
               int distance = shortestDistanceVertex.minDistance + arc.weight;
               if (arc.target.minDistance > distance) {
                  arc.target.minDistance = distance;
                  verticesQ.remove(arc.target);
                  verticesQ.add(arc.target);
                  arc.target.previous = shortestDistanceVertex;
               }
               arc = arc.next;
            }
         }
         return;

      }


      /**
       * Selle meetodi implementeerimiseks olen kasutanud https://gist.github.com/tyrantgit/8524011a158173d454fc abi.
       * Gets the shortest path from starting vertex to destination vertex. generateShortestPaths() method have to be executed first.
       * @param start From
       * @param target To
       * @return
       */
      public List<Arc> getShortestPathTo(Vertex start, Vertex target)
      {
         List<Arc> path = new ArrayList();

         for (Vertex vertex = target; !vertex.id.equals(start.id); vertex = vertex.previous) {
            path.add(new Arc(vertex.previous.id + vertex.id));
         }

         Collections.reverse(path);
         return path;
      }


      /**
       * Prints out the distance and path for given graph starting point to destination.
       * @param graph Given graph
       * @param start From
       * @param target To
       */
      public void outputShortestPathTo(Graph graph, Vertex start, Vertex target) {
         generateShortestPaths(graph, start);
         System.out.println("Let's find the shortest path in graph " + graph.id + " between vertices " + start + " and " + target + " .");
         System.out.println("Distance between vertex " + start + " and " + target + " is: " + target.minDistance);
         List<Arc> path = getShortestPathTo(start, target);
         System.out.println("The shortest path in graph " + graph.id + " between vertices " + start + " and " + target + " is: " + path);
      }
   }
}